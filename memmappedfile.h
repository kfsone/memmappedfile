#pragma once

// MemMappedFile
// Author: Oliver "kfsone" Smith 2012 <oliver@kfs.org>
// Redistribution and re-use fully permitted contingent on inclusion of these 3 lines in copied- or derived- works.

////////////////////////////////////////////////////////////////////// Doxygen documentation.
//! @class MemMappedFile
//! @brief Provide maximum performance to a file by exposing it as in-memory data.
//!
//! @detail MemMappedFile provides an abstraction layer over the
//! Windows/Linux memory-mapped file concepts.
//!
//! Essentially, eliminates the "read()" step from file IO access
//! by giving you access to the buffers used by read.
//!
//! Normally, read() has to have the OS load data from disk into
//! private buffers and then has to copy that data into user-space
//! memory (the buffers you supply). 
//!
//! Mapped files gives you protected access to those private buffers
//! in a way that provides on-demand loading of the file (presumably
//! use the system's MMU).
//!
//! Benefits:
//!  - Reduces memory pressure (no copies),
//!  - Increases cache coherency (file data in a single location),
//!  - Reduces memory usage/management overhead,
//!  - Works with the OSes disk-buffer management rather than against.
//!
//! Also implements several utility functions for dealing with text files.


////////////////////////////////////////////////////////////////////// Platform configuration.
// MEM_MAP_TYPE
//	Whether to use CreateMappedFile (_IMT_WINDOWS) or mmap (_IMT_UNIX)
// MEM_MAP_PATH_SEP_CHAR
//	Because under DOS '/' was the argument separator

#define MEM_MAP_TYPE_WINDOWS	0
#define MEM_MAP_TYPE_LINUX		1

#if defined(WIN32) || defined(WIN64) || defined(_MSC_VER)

// If you don't want to have to #include Windows.h, #define MEM_MAP_NO_WINDOWSH

//#define MEM_MAP_NO_WINDOWSH
#if defined(INVALID_HANDLE_VALUE)
#elif !defined(MEM_MAP_AVOID_WINDOWSH)
#  include <Windows.h>
#else
typedef void* HANDLE;
#define INVALID_HANDLE_VALUE ((HANDLE)(LONG_PTR)-1)
#endif

# define MEM_MAP_TYPE MEM_MAP_TYPE_WINDOWS

# define MEM_MAP_PATH_SEP_CHAR '\\'

# define MEM_MAP_MAX_PATHLEN MAX_PATH

#elif defined(__APPLE__) || defined(__GNUC__)

// Unix / Linux / Mac

# include <stdlib.h>
# include <limits.h>

# define MEM_MAP_TYPE MEM_MAP_TYPE_LINUX

# define MEM_MAP_PATH_SEP_CHAR '/'

# define MEM_MAP_MAX_PATHLEN PATH_MAX

#else

#error "Don't recognize this platform, modify the if #if conditions above."

#endif

class MemMappedFile
{
	//! Name of the file.
	char			m_filename[MEM_MAP_MAX_PATHLEN];
	//! Where the file contents actually starts.
	void*			m_basePtr;
	//! For convenience, where the file would end.
	const char*		m_endPtr;
	//! Size is required for views.
	size_t			m_size;

#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
	// For windows:
	HANDLE			m_handle;
	HANDLE			m_mapping;
#else
	int				m_fd;
#endif

public:
	MemMappedFile()
		: m_basePtr(nullptr)
		, m_endPtr(nullptr)
		, m_size(0)
#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
		, m_handle(INVALID_HANDLE_VALUE)
		, m_mapping(INVALID_HANDLE_VALUE)
#else
		, m_fd(-1)
#endif
	{
		m_filename[0] = 0;
	}

	//! Create an in-memory view of a file from disk,
	//! while trying to avoid loading it from disk.
	//! @param[in] filename_ the file/sub-path of the file to open.
	//! @param[in] dirname_ [optional] prefix path.
	MemMappedFile(const char* const filename_, const char* const dirname_ = nullptr);

	virtual ~MemMappedFile();						// Destructor.

private:
	MemMappedFile(const MemMappedFile& rhs_);			// Ownership is not transferable.
	MemMappedFile& operator = (const MemMappedFile& rhs_);

public:
	//! Check if the file mapping is open.
	//! @return true if the file has been mapped, otherwise false.
	bool good() const { return m_endPtr != nullptr; }

	//! Open a new file (closes any currently open file first).
	//! @param[in] filename_ the file/sub-path of the file to open.
	//! @param[in] dirname_ [optional] prefix path.
	//! @return true if the file was opened, false otherwise.
	bool mapFile(const char* const filename_, const char* const dirname_ = nullptr);

	//! Close and unmap the file.
	//! @return Always returns false.
	bool unmapFile();

public:
	// Accesors.

	//! Get the object's file name.
	const char* filename() const { return m_filename; }
	//! Get a pointer to the first byte of the file.
	const void* basePtr() const { return m_basePtr; }
	//! Get a pointer to the start of the file.
	const char* begin() const { return (const char*)m_basePtr; }
	//! Get a pre-calculated pointer to the end of the file (last byte + 1).
	const char* end() const { return m_endPtr; }
	//! Get the number of bytes in the file at the time it was opened.
	size_t size() const { return m_size; }
};
