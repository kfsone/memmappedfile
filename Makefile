example.exe: libmemmappedfile.a example.cpp
	$(CXX) -std=c++11 -march=native -O3 -ggdb -Wall -o example.exe example.cpp libmemmappedfile.a

libmemmappedfile.a: memmappedfile.cpp memmappedfile.h
	$(CXX) -std=c++11 -march=native -O3 -ggdb -Wall -o memmappedfile.o -c memmappedfile.cpp
	ar rcs libmemmappedfile.a memmappedfile.o

clean:
	rm libmemmappedfile.a memmappedfile.o example.exe

