// MemMappedFile
// Author: Oliver "kfsone" Smith 2012 <oliver@kfs.org>
// Redistribution and re-use fully permitted contingent on inclusion of these 3 lines in copied- or derived- works.

#include "memmappedfile.h"

#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
# define WIN32_LEAN_AND_MEAN
# include <Windows.h>
#else
# include <sys/mman.h>	// Some systems use mman.h instead.
# include <sys/types.h>
# include <sys/stat.h>
# include <errno.h>
# include <fcntl.h>
# include <unistd.h>
#endif

//////////////////////////////////////////////////////////////////////
// Constructor (Posix + Windows versions combined)
// Note: Posix doesn't support wchar_t filenames.

MemMappedFile::MemMappedFile(const char* const filename_, const char* const dirname_)
	: m_basePtr(nullptr)
	, m_endPtr(nullptr)
	, m_size(0)
#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
	, m_handle(INVALID_HANDLE_VALUE)
	, m_mapping(INVALID_HANDLE_VALUE)
#else // Unix uses a file descriptor.
	, m_fd(-1)
#endif
{
	m_filename[0] = 0;
	mapFile(filename_, dirname_);
}

//////////////////////////////////////////////////////////////////////
// Destructor.

MemMappedFile::~MemMappedFile()
{
	unmapFile();
}

//////////////////////////////////////////////////////////////////////
// Populate filename with the combination of dirname and filename.

static void _populateFilename(char* const dest, const char* const filename, const char* const dirname)
{
	char* const end = dest + MEM_MAP_MAX_PATHLEN;
	char* ptr = dest;
	if (dirname != nullptr && dirname[0] != 0) {
		for (const char* src = dirname; *src != 0 && ptr < end;)
			*(ptr++) = *(src++);
		if (*(ptr - 1) != MEM_MAP_PATH_SEP_CHAR)
			*(ptr++) = MEM_MAP_PATH_SEP_CHAR;
	}

	for (const char* src = filename; *src != 0 && ptr < end;)
		*(ptr++) = *(src++);
	*ptr = 0;
}

//////////////////////////////////////////////////////////////////////
// Attempt to open a file. Returns false on error.

bool MemMappedFile::mapFile(const char* const filename_, const char* const dirname_)
{
	// Make sure we close any currently-open/mapped file.
	unmapFile();

	_populateFilename(m_filename, filename_, dirname_);

	// Check we can open the file and stat it for size.
#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
	// Windows implementation.
	m_handle = CreateFileA(m_filename, FILE_GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
	if (m_handle == INVALID_HANDLE_VALUE)
		return false;

	// Determine file size.
	LARGE_INTEGER size;
	if (!GetFileSizeEx(m_handle, &size))
		return unmapFile();

	m_size = (size_t)size.QuadPart;
#else
	// Posix implementation.
	const int fd = open(m_filename, O_RDONLY);
	if (fd < 0)
		return unmapFile();
	m_fd = fd;

	// Use fstat to quickly obtain the file size.
	struct stat stats;
	const int sr = fstat(fd, &stats);
	if (sr < 0)
		return unmapFile();
	m_size = stats.st_size;
#endif

	// Empty file? Don't try and mmap it then.
	if (m_size == 0)
		return true;

	// Ask the OS to provide an in-memory view of the data; which is
	// basically saying "load this file into buffers like you would,
	// but then give us direct access to the buffer memory".
#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
	// Windows implementation.
	m_mapping = CreateFileMapping(m_handle, nullptr, PAGE_READONLY, 0, 0, nullptr);
	if (m_mapping == NULL)
		return unmapFile();

	LPVOID const ptr = MapViewOfFileEx(m_mapping, FILE_MAP_READ, 0, 0, 0, nullptr);
	if (ptr == nullptr)
		return unmapFile();
#else
	// Posix implementation.
	static const int flags =
		MAP_FILE 		// Compatability flag, not really required.
		| MAP_SHARED	// Share buffers with any other mmappers of this file.
		;
	void* const ptr =
		mmap(nullptr, m_size + 1, PROT_READ, flags, m_fd, 0);
	if (ptr == MAP_FAILED)
		return unmapFile();
#endif

	// Both methods return a pointer to the beginning of the
	// OS'es internal buffers. There may not be any data there,
	// and the first access may result in a page fault (the
	// OS has to actually fetch data, akin to the first call
	// of read()).
	m_basePtr = (void*)ptr;

	// For convenience, pre-calculate where the end of the data is.
	m_endPtr = (const char*)m_basePtr + m_size;

	return true;
}

//////////////////////////////////////////////////////////////////////
// Close and unmap the mapping.

bool MemMappedFile::unmapFile()
{
	if (m_basePtr != nullptr) {
#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
		UnmapViewOfFile(m_basePtr);
#else
		munmap(m_basePtr, m_size + 1);
#endif

		m_basePtr = nullptr;
		m_endPtr = nullptr;
		m_size = 0;
	}

#if MEM_MAP_TYPE == MEM_MAP_TYPE_WINDOWS
	if (m_mapping != INVALID_HANDLE_VALUE) {
		CloseHandle(m_mapping);
		m_mapping = INVALID_HANDLE_VALUE;
	}
	if (m_handle != INVALID_HANDLE_VALUE) {
		CloseHandle(m_handle);
		m_handle = INVALID_HANDLE_VALUE;
	}
#else
	if (m_filename[0] != 0 && m_fd >= 0) {
		close(m_fd);
		m_fd = -1;
	}
#endif

	return false;
}
