// MemMappedFile Example Usage
// Author: Oliver "kfsone" Smith 2013 <oliver@kfs.org>
// Redistribution and re-use fully permitted contingent on inclusion of these 3 lines in copied- or derived- works.

// Demonstration of using MemMappedFile:
// Builds "example.exe" which will take 1 or more filenames as arguments,
// each file will be opened using memory-mapped file operations and
// then we will count the number of times 'x' occurs in the file and
// produce a hash of the same file.

// NOTE: Compile with optimization enabled.

#if defined(WIN32) || defined(WIN64) || defined(_MSC_VER)
#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

#include "memmappedfile.h"

#include <iostream>
#include <fstream>
#include <functional>
#include <string.h>

size_t useMemMap(const char* filename, unsigned int& xCount, unsigned int& hash)
{
	MemMappedFile file(filename);
	if (!file.good()) {
		std::cout << "Unable to open file." << std::endl;
		return 0;
	}

	for (const char* cur = file.begin(); cur != file.end(); ++cur) {
		xCount += (*cur == 'x') ? 1 : 0;
		hash = (((hash >> 27) % 7) | (hash << 6)) ^ *cur;
	}

	return file.size();
}

size_t useReadChar(const char* filename, unsigned int& xCount, unsigned int& hash)
{
	std::ifstream file(filename, std::ifstream::in | std::ifstream::binary);
	if (!file.good()) {
		std::cout << "Unable to open file." << std::endl;
		return 0;
	}

	char cur;
	size_t size = 0;
	while (file.good()) {
		file >> cur;
		xCount += (cur == 'x') ? 1 : 0;
		hash = (((hash >> 27) % 7) | (hash << 6)) ^ cur;
		++size;
	}

	return size;
}

size_t useReadPage(const char* filename, unsigned int& xCount, unsigned int& hash)
{
	FILE* file = fopen(filename, "rb");
	if (!file) {
		std::cout << "Unable to open file." << std::endl;
		return 0;
	}

	char page[4096];
	size_t size = 0;
	for (;;) {
		size_t bytesRead = fread(page, sizeof(page[0]), sizeof(page) / sizeof(page[0]), file);
		if (bytesRead == 0)
			break;
		for (size_t i = 0; i < bytesRead; ++i) {
			xCount += (page[i] == 'x') ? 1 : 0;
			hash = (((hash >> 27) % 7) | (hash << 6)) ^ page[i];
		}
		size += bytesRead;
	}

	fclose(file);

	return size;
}

enum { MODE_MEMMAP, MODE_BY_CHAR, MODE_BY_PAGE, NUM_MODES };
static const char* modeNames[NUM_MODES] = { "--memmap", "--bychar", "--bypage" };
static std::function<size_t(const char* filename, unsigned int& xCount, unsigned int& hash)> modeFunctions[3] = { useMemMap, useReadChar, useReadPage };

int main(int argc, const char* argv[])
{
	if (argc < 1) {
		std::cerr << "Usage: " << argv[0] << " [--bychar | --bypage] filename1 [... filenameN]" << std::endl;
		std::cerr << "Demonstrates using Memory Mapped File operations to read one or more files, counting the number of 'x's and calculating a hash for each file." << std::endl;
		std::cerr << "  --memmap   uses mem mapped file (default)." << std::endl;
		std::cerr << "  --bychar   reads the file char by char." << std::endl;
		std::cerr << "  --bypage   reads the file page by page." << std::endl;
		return 1;
	}

	int mode = 0;
	for (int argn = 1; argn < argc; ++argn) {
		const char* const arg = argv[argn];
		if (arg[0] == '-') {
			mode = -1;
			for (size_t i = 0; i < NUM_MODES; ++i) {
				if (strcmp(arg, modeNames[i]) == 0) {
					mode = i;
					break;
				}
			}
			if (mode == -1) {
				std::cerr << "Unrecognized argument, '" << arg << "'" << std::endl;
				return 1;
			}
		} else {
			std::cout << arg << ": ";
			unsigned int xCount = 0;
			unsigned int hash = 0xf1f4f3f7;
			size_t size = modeFunctions[mode](arg, xCount, hash);
			if (size > 0)
				std::cout << size << " bytes, " << xCount << " 'x's, " << hash << " hash." << std::endl;
		}
	}

	return 0;
}