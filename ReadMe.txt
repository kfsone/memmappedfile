MemMappedFile by Oliver "kfsone" Smith <oliver@kfs.org> 2012
------------------------------------------------------------

Simple cross-platform (Windows, Linux, Mac) wrapper class for basic Memory Mapped file reading operations (Windows' Map ViewOfFileEx, Lin/Mac mmap).

Most modern operating systems actually retrieve disk data in multiples of a page and keep recent pages in a cache. Memory mapping a file allows you to access this cache, greatly reducing the cost of accessing the file.

Conventional code:

    void test(const char* filename)
    {
        ifstream f(filename);
        if (!f.good())
            return;

        size_t bytesInFile = getFileSize(f);
        if (bytesInFile <= 0)
            return;

        char* data = new data[bytesInFile];
        const char* end = data + bytesInFile;

        uint64_t xCount = 0;
        for (const char* p = data; p != end; ++p) {
            if (*p == 'x')
                ++xCount;
        }

        std::cout << filename << " contains " << xCount << " 'x's" << std::endl;

        delete [] data;
    }

Using MemMappedFile does not require allocation or copying and provides random access. If you access an area of the file that the OS has not yet loaded, the read access will be delayed while the OS fetches it.

    void test(const char* filename)
    {
        MemMappedFile f(filename);
        if (!f.good() || !f.size())
            return;

        uint64_t xCount = 0;
        for (const char* p = file.begin(); p != file.end(); ++p) {
            if (*p == 'x')
                ++xCount;
        }

        std::cout << filename << " contains " << xCount << " 'x's" << std::endl;
    }

MemMappedFile has a relatively simple interface:

	MemMappedFile();
		Constructs an empty MemMappedFile instance for future mapping.
		Calling file.good() will return false until the file is mapped.

	MemMappedFile(const char* filename);
	MemMappedFile(const char* filename, const char* pathName);
		Constructs a MemMappedFile instance that attempts to map the
		specified file. If a pathname is specified, it will be combined
		with the filename before attempting to map.
		Calling file.good() will return true if the map is successful.

	bool mapFile(const char* filename);
	bool mapFile(const char* filename, const char* pathName);
		Attempts to map the specified file, in the specified directory if
		pathname is supplied. If the object already has a file mapping,
		the previous file is first unmapped.
		Returns true if the map is successful.

	bool unmapFile()
		If the object has a file mapped, unmaps the file and returns true.
		Otherwise returns false.

	bool good()
		Returns true if the instance has a file mapped, otherwise false.

	const char* filename()
		Returns the path + filename of the last file mapped.

	const void* basePtr()
		If a file is mapped, returns the address of the start of the file data.
		Otherwise returns nullptr.
		
	const char* begin()
		If a file is mapped, returns the address of the start of the file data.
		Otherwise returns nullptr.
		
	const char* end()
		If a file is mapped, returns the address of the end of the file data,
		i.e. the last byte + 1.
		Otherwise returns nullptr.

	size_t size()
		If a file is mapped, returns the size in bytes of the file.
		Otherwise returns 0.

See "example.cpp" for an example and comparison against other IO methods.
